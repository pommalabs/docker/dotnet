#!/bin/bash
set -e

source /opt/bash/common_commands.sh

# Make sure that PowerShell is installed.
pwsh --version

# Library fix for libgit2.
$SLIM_INSTALL libgit2-dev
ln -s /usr/lib/x86_64-linux-gnu/libgit2.so /lib/x86_64-linux-gnu/libgit2-15e1193.so
