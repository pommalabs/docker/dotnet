#!/bin/bash
set -e

source /opt/bash/common_commands.sh

# Install Node.js LTS with build tools.
$SLIM_INSTALL gnupg
mkdir -p /etc/apt/keyrings
$SAFE_CURL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor --output /etc/apt/keyrings/nodesource.gpg
echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_VERSION nodistro main" > /etc/apt/sources.list.d/nodesource.list
apt-get update -qq
$SLIM_INSTALL build-essential nodejs
node --version && npm --version

# Install Yarn.
npm install --global yarn
yarn --version
