#!/bin/bash
set -e

source /opt/bash/common_commands.sh

# Create an application user with limited privileges.
# Starting from .NET 8, a non-root user is already available
# within standard image: therefore, we need to check its existence
# before trying to create it.
id -u app || useradd --create-home --uid $APP_UID app

# Create application directory.
mkdir -p /opt/app

# Disable "su" for all users, except those who belong to the root group.
echo "auth required pam_wheel.so" >> /etc/pam.d/su
