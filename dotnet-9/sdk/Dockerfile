FROM mcr.microsoft.com/dotnet/sdk:9.0-bookworm-slim

ARG description="Docker image with .NET SDK, based on Debian Linux"
LABEL description=${description} org.opencontainers.image.description=${description}

ENV \
    # Add SQL Server and dotnet tools to path.
    PATH="${PATH}:/opt/mssql-tools/bin:/root/.dotnet/tools" \
    # Enable .NET minor and major version roll-forward behavior.
    DOTNET_ROLL_FORWARD_ON_NO_CANDIDATE_FX=2

COPY ./bash /opt/bash
RUN export MS_REPO_DISTRO="debian/12" && \
    export NODE_VERSION="22.x" && \
    bash /opt/bash/common_setup.sh && \
    bash /opt/bash/sdk_dotnet.sh && \
    bash /opt/bash/sdk_node.sh && \
    bash /opt/bash/sdk_sql_clients.sh && \
    bash /opt/bash/common_cleanup.sh && \
    rm -rf /opt/bash

WORKDIR /opt/sln
