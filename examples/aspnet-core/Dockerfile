FROM container-registry.pommalabs.xyz/pommalabs/dotnet:8-aspnet AS base
# Instead of: mcr.microsoft.com/dotnet/aspnet:8.0

FROM container-registry.pommalabs.xyz/pommalabs/dotnet:8-sdk AS build
# Instead of: mcr.microsoft.com/dotnet/sdk:8.0
ARG BUILD_CONFIGURATION=Release
COPY ["example.csproj", ""]
RUN dotnet restore "./example.csproj"
COPY . .
WORKDIR "/opt/sln/"
RUN dotnet build "example.csproj" -c ${BUILD_CONFIGURATION} -o /opt/app/build

FROM build AS publish
ARG BUILD_CONFIGURATION=Release
RUN dotnet publish "example.csproj" -c ${BUILD_CONFIGURATION} -o /opt/app/publish /p:UseAppHost=false

FROM base AS final
COPY --from=publish /opt/app/publish .
# Use CMD instead of ENTRYPOINT, which is defined by custom image.
CMD ["dotnet", "example.dll"]