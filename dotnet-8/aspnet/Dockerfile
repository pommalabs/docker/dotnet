FROM mcr.microsoft.com/dotnet/aspnet:8.0-bookworm-slim

ARG description="Docker image with ASP.NET Core, based on Debian Linux"
LABEL description=${description} org.opencontainers.image.description=${description}

# Use ports which do not require root user.
EXPOSE 8080

ENV PORT=8080 \
    # Tell ASP.NET Core to use a fixed port.
    ASPNETCORE_HTTP_PORTS=${PORT}

# Custom scripts.
COPY ./bash /opt/bash
RUN bash /opt/bash/common_setup.sh && \
    bash /opt/bash/aspnet_setup.sh && \
    bash /opt/bash/aspnet_security.sh && \
    bash /opt/bash/common_cleanup.sh && \
    rm -rf /opt/bash

WORKDIR /opt/app

# Run as an unprivileged application user. By specifying the numeric UID,
# "runAsNonRoot" flag within Kubernetes security context can be set to true.
USER $APP_UID

# Run dumb-init as main process (https://github.com/Yelp/dumb-init).
ENTRYPOINT ["dumb-init", "--"]
